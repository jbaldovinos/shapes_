import math
from abc import abstractmethod


class Operaciones():
    @abstractmethod
    def perimetros(self):
        perimetro = 0

    @abstractmethod
    def areas(self):
        area = 0


class Geometric:
    hipotenusa = 0
    radio = 0

    def area_triangulo(self, base, altura):
        self.area = base * altura / 2

    def perimetro_triangulo(self, base, altura):
        self.hipotenusa = math.sqrt(((base * base) + (altura * altura)))
        self.perimetro = (base + altura + self.hipotenusa)

    def area_cuadrado(self, base):
        self.area = (base * base)

    def perimetro_cuadrado(self, base):
        self.perimetro = int(base * 4)

    def area_circulo(self, radio):
        self.area = math.pi * (radio * radio)

    def perimetro_circulo(self, radio):
        self.perimetro = math.pi * 2 * radio
