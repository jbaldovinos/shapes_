import unittest
from geometric import Geometric


class TestTriangulo(unittest.TestCase):

    def setUp(self) -> None:
        self.geometric = Geometric()

    def test_intance(self):
        print("Instance")
        self.assertNotEqual(self.geometric, None)

    def test_default_value(self):
        print("test_default_value")
        self.assertNotEqual(self.geometric.area_triangulo, 0)

    def test_areaTriangulo(self):
        self.geometric.area_triangulo(12, 15)
        self.assertEqual(self.geometric.area, 90)

    def test_perimetroTriangulo(self):
        self.geometric.perimetro_triangulo(12, 15)
        self.assertEqual(round(self.geometric.perimetro), 46)


