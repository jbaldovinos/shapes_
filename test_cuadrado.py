import unittest
from geometric import Geometric


class TestCuadrado(unittest.TestCase):

    def setUp(self) -> None:
        self.geometric = Geometric()

    def test_intance(self):
        print("Instance")
        self.assertNotEqual(self.geometric, None)

    def test_default_value(self):
        print("test_default_value")
        self.assertNotEqual(self.geometric.area_cuadrado, 0)

    def test_areaCuadrado(self):
        self.geometric.area_cuadrado(5)
        self.assertEqual(self.geometric.area, 25)

    def test_perimetroTriangulo(self):
        self.geometric.perimetro_cuadrado(5)
        self.assertEqual(self.geometric.perimetro, 20)
