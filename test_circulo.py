import unittest

from geometric import Geometric


class TestCirculo(unittest.TestCase):

    def setUp(self) -> None:
        self.geometric = Geometric()

    def test_intance(self):
        print("Instance")
        self.assertNotEqual(self.geometric, None)

    def test_default_value(self):
        print("test_default_value")
        self.assertNotEqual(self.geometric.area_circulo, 0)

    def test_areaCirculo(self):
        self.geometric.area_circulo(3)
        self.assertEqual(round(self.geometric.area), 28)

    def test_perimetroCirculo(self):
        self.geometric.perimetro_circulo(3)
        self.assertEqual((round(self.geometric.perimetro)), 19)


